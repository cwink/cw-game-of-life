#include <SDL2/SDL.h>

#include <algorithm>
#include <iostream>
#include <random>
#include <stdexcept>
#include <vector>

namespace {
constexpr auto milliseconds_to_draw = 250;

constexpr auto main_window_width = 640;

constexpr auto main_window_height = 480;

constexpr auto main_window_divisor = 4;

constexpr auto main_window_title = "CW Game Of Life";

constexpr auto frames_per_second = 60.0;
} // namespace

namespace {
class Sdl final {
public:
  Sdl() = delete;

  Sdl(const std::string &window_title, const std::size_t window_width, const std::size_t window_height,
      const std::size_t window_divisor) {
    if (window_width % 2 != 0 || window_height % 2 != 0) {
      throw std::runtime_error("Width and height must be divisable by 2.");
    }

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
      throw std::runtime_error(SDL_GetError());
    }

    this->window_ = SDL_CreateWindow(window_title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, // NOLINT
                                     static_cast<int>(window_width), static_cast<int>(window_height), 0);

    if (this->window_ == nullptr) {
      throw std::runtime_error(SDL_GetError());
    }

    this->renderer_ = SDL_CreateRenderer(this->window_, -1, SDL_RENDERER_ACCELERATED);

    if (this->renderer_ == nullptr) {
      throw std::runtime_error(SDL_GetError());
    }

    if (SDL_RenderSetIntegerScale(this->renderer_, SDL_TRUE) < 0) {
      throw std::runtime_error(SDL_GetError());
    }

    const auto viewport = SDL_Rect{0, 0, static_cast<int>(window_width), static_cast<int>(window_height)};

    if (SDL_RenderSetViewport(this->renderer_, &viewport) < 0) {
      throw std::runtime_error(SDL_GetError());
    }

    if (SDL_RenderSetLogicalSize(this->renderer_, static_cast<int>(window_width / window_divisor),
                                 static_cast<int>(window_height / window_divisor)) < 0) {
      throw std::runtime_error(SDL_GetError());
    }

    this->width_ = window_width / window_divisor;

    this->height_ = window_height / window_divisor;
  }

  Sdl(const Sdl &sdl) = delete;

  [[maybe_unused]] Sdl(Sdl &&sdl) noexcept = default;

  auto operator=(const Sdl &sdl) -> Sdl & = delete;

  [[maybe_unused]] auto operator=(Sdl &&sdl) noexcept -> Sdl & = default;

  ~Sdl() {
    if (this->renderer_ != nullptr) {
      SDL_DestroyRenderer(this->renderer_);
    }

    if (this->window_ != nullptr) {
      SDL_DestroyWindow(this->window_);
    }

    SDL_Quit();
  }

  [[maybe_unused]] [[nodiscard]] auto window() const -> const SDL_Window * { return this->window_; }

  [[maybe_unused]] [[nodiscard]] auto window() -> SDL_Window * { return this->window_; }

  [[maybe_unused]] [[nodiscard]] auto renderer() const -> const SDL_Renderer * { return this->renderer_; }

  [[maybe_unused]] [[nodiscard]] auto renderer() -> SDL_Renderer * { return this->renderer_; }

  [[maybe_unused]] [[nodiscard]] auto width() const -> std::size_t { return this->width_; }

  [[maybe_unused]] [[nodiscard]] auto height() const -> std::size_t { return this->height_; }

private:
  SDL_Window *window_;

  SDL_Renderer *renderer_;

  std::size_t width_;

  std::size_t height_;
};

auto render_clear(Sdl &sdl, const std::uint8_t red, const std::uint8_t green, const std::uint8_t blue) -> void {
  if (SDL_SetRenderDrawColor(sdl.renderer(), red, green, blue, 255) < 0) { // NOLINT
    throw std::runtime_error(SDL_GetError());
  }

  if (SDL_RenderClear(sdl.renderer()) < 0) {
    throw std::runtime_error(SDL_GetError());
  }
}

auto render_present(Sdl &sdl) -> void { SDL_RenderPresent(sdl.renderer()); }

auto render_points(Sdl &sdl, const std::vector<SDL_Point> &points, const std::uint8_t red, const std::uint8_t green,
                   const std::uint8_t blue) -> void {
  if (std::any_of(std::cbegin(points), std::cend(points), [&](const auto &point) {
        return point.x < 0 || point.y < 0 || point.x >= static_cast<int>(sdl.width()) ||
               point.y >= static_cast<int>(sdl.height());
      })) {
    throw std::runtime_error("X or Y point is out of viewport bounds.");
  }

  if (SDL_SetRenderDrawColor(sdl.renderer(), red, green, blue, 255) < 0) { // NOLINT
    throw std::runtime_error(SDL_GetError());
  }

  if (SDL_RenderDrawPoints(sdl.renderer(), points.data(), static_cast<int>(points.size())) < 0) {
    throw std::runtime_error(SDL_GetError());
  }
}

template <typename Event_callback, typename Update_callback, typename Render_callback>
auto start(Sdl &sdl, const std::uint8_t red, const std::uint8_t green, std::uint8_t blue, const double fps,
           const Event_callback &event_callback, const Update_callback &update_callback,
           const Render_callback &render_callback) -> void {
  auto time = 0.0;

  const auto delta_time = 1.0 / fps;

  auto current_time = static_cast<double>(SDL_GetTicks()) / 0.001; // NOLINT

  auto accumulator = 0.0;

  auto running = true;

  while (running) {
    auto event = SDL_Event{};

    while (SDL_PollEvent(&event)) {
      running = event_callback(event);

      if (!running) {
        break;
      }
    }

    if (!running) {
      break;
    }

    const auto new_time = static_cast<double>(SDL_GetTicks()) / 0.001; // NOLINT

    const auto frame_time = new_time - current_time;

    current_time = new_time;

    accumulator += frame_time;

    while (accumulator >= delta_time) {
      update_callback(time, delta_time);

      accumulator -= delta_time;

      time += delta_time;
    }

    render_clear(sdl, red, green, blue);

    render_callback();

    render_present(sdl);

    SDL_Delay(1);
  }
}

[[nodiscard]] inline auto random_unsigned_integer(const std::uint32_t minimum, const std::uint32_t maximum)
    -> std::uint32_t {
  auto rand_device = std::random_device();

  auto engine = std::mt19937(rand_device());

  auto distribution = std::uniform_int_distribution<std::uint32_t>(minimum, maximum);

  return distribution(engine);
}
} // namespace

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int {
  try {
    auto sdl = Sdl(main_window_title, main_window_width, main_window_height, main_window_divisor);

    auto canvas = std::vector<bool>(sdl.width() * sdl.height(), false);

    for (auto counter = canvas.size() / 8; counter < canvas.size() / 4; ++counter) { // NOLINT
      canvas[random_unsigned_integer(0, static_cast<std::uint32_t>(canvas.size()) - 1)] = true;
    }

    auto canvas_buffer = std::vector<bool>(sdl.width() * sdl.height(), false);

    auto points = std::vector<SDL_Point>();

    auto time_tracker = static_cast<std::uint32_t>(0);

    start(
        sdl, 0, 0, 0, frames_per_second,
        [&](const auto &event) {
          switch (event.type) { // NOLINT
          case SDL_WINDOWEVENT:
            switch (event.window.event) { // NOLINT
            case SDL_WINDOWEVENT_CLOSE:
              return false;

              break;
            default:

              break;
            }

            break;
          case SDL_KEYDOWN:
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch-enum"
            switch (event.key.keysym.scancode) { // NOLINT
#pragma GCC diagnostic pop
            case SDL_SCANCODE_ESCAPE:
              return false;

              break;
            default:

              break;
            }

            break;
          }

          return true;
        },
        [&]([[maybe_unused]] const auto time, [[maybe_unused]] const auto delta_time) {},
        [&]() {
          if (SDL_GetTicks() - time_tracker > milliseconds_to_draw) {
            points.clear();

            for (auto y = 0; y < static_cast<int>(sdl.height()); ++y) {
              for (auto x = 0; x < static_cast<int>(sdl.width()); ++x) {
                auto count = static_cast<std::size_t>(0);

                for (auto dx = x - 1; dx <= x + 1; ++dx) {
                  for (auto dy = y - 1; dy <= y + 1; ++dy) {
                    if (x == dx && y == dy) {
                      continue;
                    }

                    const auto p = dy * static_cast<int>(sdl.width()) + dx;

                    if (p >= 0 && p < static_cast<int>(canvas.size()) && canvas[static_cast<std::size_t>(p)]) {
                      ++count;
                    }
                  }
                }

                const auto p = static_cast<std::size_t>(y * static_cast<int>(sdl.width()) + x); // NOLINT

                if (canvas[p]) {
                  points.emplace_back(SDL_Point{x, y});
                }

                if (canvas[p] && (count < 2 || count > 3)) {
                  canvas_buffer[p] = false;

                  continue;
                }

                if ((canvas[p] && (count == 2 || count == 3)) || (!canvas[p] && count == 3)) {
                  canvas_buffer[p] = true;

                  continue;
                }

                canvas_buffer[p] = false;
              }
            }

            std::copy(std::cbegin(canvas_buffer), std::cend(canvas_buffer), std::begin(canvas));

            time_tracker = SDL_GetTicks();
          }

          if (!points.empty()) {
            render_points(sdl, points, 255, 0, 0); // NOLINT
          }
        });
  } catch (const std::exception &ex) {
    std::cerr << ex.what() << std::endl;

    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
